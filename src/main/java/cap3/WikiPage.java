package cap3;

public class WikiPage {
    private final String render;
    private boolean hasSuiteSetup;
    private boolean hasSetup;
    private boolean tearDown;
    private boolean hasSuiteTearDown;

    public WikiPage(final boolean hasSuiteSetup, final boolean hasSetup, final String render, final boolean tearDown, final boolean hasSuiteTearDown) {
        this.hasSuiteSetup = hasSuiteSetup;
        this.hasSetup = hasSetup;
        this.render = render;
        this.tearDown = tearDown;
        this.hasSuiteTearDown = hasSuiteTearDown;
    }

    public PageCrawler getPageCrawler() {
        return new PageCrawlerImpl(render);
    }

    public boolean hasSuiteSetup() {
        return this.hasSuiteSetup;
    }

    public boolean hasSetup() {
        return hasSetup;
    }

    public boolean tearDown() {
        return tearDown;
    }

    public boolean hasSuiteTtearDown() {
        return hasSuiteTearDown;
    }
}
