package cap3;

public class WikiPagePath {
    private final String render;

    public WikiPagePath(final String render) {
        this.render = render;
    }

    public String toString() {
        return render;
    }
}
