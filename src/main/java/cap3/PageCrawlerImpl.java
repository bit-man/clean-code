package cap3;

import static cap3.SuiteResponder.SUITE_SETUP_NAME;
import static cap3.SuiteResponder.SUITE_TEARDOWN_NAME;

public class PageCrawlerImpl implements PageCrawler {
    private final String render;

    public PageCrawlerImpl(final String render) {
        this.render = render;
    }

    public static WikiPage getInheritedPage(final String suiteSetupName, final WikiPage wikiPage) {
        switch (suiteSetupName) {
            case SUITE_SETUP_NAME:
                if (wikiPage.hasSuiteSetup()) {
                    return new WikiPage(false, false, "AAAAA", true, true);
                } else {
                    return null;
                }
            case "SetUp":
                if (wikiPage.hasSetup()) {
                    return new WikiPage(false, false, "BBBBB", true, true);
                } else {
                    return null;
                }
            case "TearDown":
                if (wikiPage.tearDown()) {
                    return new WikiPage(false, false, "CCCCC", true, true);
                } else {
                    return null;
                }
            case SUITE_TEARDOWN_NAME:
                if (wikiPage.hasSuiteTtearDown()) {
                    return new WikiPage(false, false, "DDDDD", true, true);
                } else {
                    return null;
                }
        }
        throw  new UnsupportedOperationException();
    }

    @Override
    public WikiPagePath getFullPath(final WikiPage suiteSetup) {
        return new WikiPagePath(render);
    }
}
