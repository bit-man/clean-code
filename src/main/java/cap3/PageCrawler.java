package cap3;

public interface PageCrawler {
    WikiPagePath getFullPath(WikiPage suiteSetup);
}
