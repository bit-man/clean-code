package cap3;

public class PageData {
    private final boolean hasSetup;
    private String content;
    private final boolean hasTestAttribute;
    private final boolean hasSuiteSetup;
    private final boolean tearDown;
    private final boolean hasSuiteTearDown;

    public PageData(String content, final boolean hasTestAttribute, final boolean hasSuiteSetup, final boolean hasSetup, final boolean tearDown, final boolean hasSuiteTearDown) {
        this.content = content;
        this.hasTestAttribute = hasTestAttribute;
        this.hasSuiteSetup = hasSuiteSetup;
        this.hasSetup = hasSetup;
        this.tearDown = tearDown;
        this.hasSuiteTearDown = hasSuiteTearDown;
    }

    public WikiPage getWikiPage() {
        return new WikiPage(hasSuiteSetup, hasSetup, "BBBBB", tearDown,hasSuiteTearDown );
    }

    public boolean hasAttribute(final String test) {
        return hasTestAttribute;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public String getHtml() {
        return "<html>" + content + "</html>";
    }
}
