package cap3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FitnesseShould {
    private PageData pageData;
    private boolean includeSuiteSetup;
    private boolean hasSuiteSetup;
    private boolean hasTestAttribute;
    private String html;
    private boolean hasSetup;
    private boolean hasTearDown;
    private boolean hasSuiteTearDown;

    @Test
    public void pageDataHasNoTestAttribute() throws Exception {
        hasTestAttribute = false;
        includeSuiteSetup = false;
        hasSuiteSetup = false;
        givenPageData();

        whenTestingTable();

        thenContentIs("<html>PPPP</html>");
    }

    @Test
    public void pageDataHasTestAttributeButNeitherIncludeSuiteSetupNorHasSuiteSetup() throws Exception {
        hasTestAttribute = true;
        includeSuiteSetup = false;
        hasSuiteSetup = false;
        hasSetup = false;
        givenPageData();

        whenTestingTable();

        thenContentIs("<html>PPPP</html>");
    }

    @Test
    public void pageDataHasTestAttributeIncludeSuiteSetupButHasNoSuiteSetup() throws Exception {
        hasTestAttribute = true;
        includeSuiteSetup = true;
        hasSuiteSetup = false;
        hasSetup = false;
        givenPageData();

        whenTestingTable();

        thenContentIs("<html>PPPP</html>");
    }

    @Test
    public void pageDataHasTestAttributeIncludeSuiteSetupAndHasSuiteSetupButNoSetup() throws Exception {
        hasTestAttribute = true;
        includeSuiteSetup = true;
        hasSuiteSetup = true;
        hasSetup = false;
        givenPageData();

        whenTestingTable();

        thenContentIs("<html>!include -setup .AAAAA\n" +
                "PPPP</html>");
    }

    @Test
    public void pageDataHasTestAttributeIncludeSuiteSetupAndHasSuiteSetupAndSetupButNotTearDown() throws Exception {
        hasTestAttribute = true;
        includeSuiteSetup = true;
        hasSuiteSetup = true;
        hasSetup = true;
        hasTearDown = false;
        givenPageData();

        whenTestingTable();

        thenContentIs("<html>!include -setup .AAAAA\n" +
                "!include -setup .BBBBB\n" +
                "PPPP</html>");
    }

    @Test
    public void pageDataHasTestAttributeIncludeSuiteSetupAndHasSuiteSetupAndSetupAndTeardown() throws Exception {
        hasTestAttribute = true;
        includeSuiteSetup = true;
        hasSuiteSetup = true;
        hasSetup = true;
        hasTearDown = true;
        hasSuiteTearDown = false;
        givenPageData();

        whenTestingTable();

        thenContentIs("<html>!include -setup .AAAAA\n" +
                "!include -setup .BBBBB\n" +
                "PPPP\n" +
                "!include -teardown .BBBBB\n" +
                "</html>");
    }

    @Test
    public void pageDataHasTestAttributeIncludeSuiteSetupAndHasSuiteSetupAndSetupAndTeardownAndSuiteTearDown() throws Exception {
        hasTestAttribute = true;
        includeSuiteSetup = true;
        hasSuiteSetup = true;
        hasSetup = true;
        hasTearDown = true;
        hasSuiteTearDown = true;
        givenPageData();

        whenTestingTable();

        thenContentIs("<html>!include -setup .AAAAA\n" +
                "!include -setup .BBBBB\n" +
                "PPPP\n" +
                "!include -teardown .BBBBB\n" +
                "!include -teardown .DDDDD\n" +
                "</html>");
    }

    private void thenContentIs(final String expected) {
        assertEquals(expected, html);
    }

    private void whenTestingTable() throws Exception {
        html = Fitnesse.testableHtml(pageData, includeSuiteSetup);
    }

    private void givenPageData() {
        pageData = new PageData("PPPP", hasTestAttribute, hasSuiteSetup, hasSetup, hasTearDown, hasSuiteTearDown);
    }
}
